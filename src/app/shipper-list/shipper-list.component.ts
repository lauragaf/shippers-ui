import { Component, OnInit } from '@angular/core';
import { RestApiService } from "../shared/rest-api.service";


@Component({
  selector: 'app-shipper-list',
  templateUrl: './shipper-list.component.html',
  styleUrls: ['./shipper-list.component.css']
})
export class ShipperListComponent implements OnInit {

  Shippers: any = [];
  constructor( 
    public restApi: RestApiService
    ) { }

  ngOnInit(): void {
    this.loadShippers()
  }

  loadShippers() {
    return this.restApi.getShippers().subscribe((data: {}) => {
        this.Shippers = data;
    })
  }

  deleteShipper(id:any) {
    if (window.confirm('Are you sure, you want to delete?')){
      this.restApi.deleteShipper(id).subscribe(data => {
        this.loadShippers()
      })
    }
  }  

}


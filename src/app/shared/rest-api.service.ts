import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {retry, catchError } from 'rxjs/operators';
import {Shipper } from './shipper';
import { Observable, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class RestApiService {

  apiURL = environment.apiURL;

  constructor(private http: HttpClient) { }

  //http options
  httpOptions = {
    headers: new HttpHeaders ({
      'Content-Type' : 'application/json'
    })
  }

  //httpclient api get() method
  getShippers(): Observable<Shipper>{
    return this.http.get<Shipper>(this.apiURL)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }
//get api id
  getShipper(id:any): Observable<Shipper> {
    return this.http.get<Shipper>(this.apiURL + id)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  createShipper(shipper:Shipper): Observable<Shipper>{
    return this.http.post<Shipper>(this.apiURL + '', JSON.stringify(shipper), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )

  }


// HttpClient API put() method
  updateShipper(id:number, shipper:Shipper): Observable<Shipper> {
    return this.http.put<Shipper>(this.apiURL + '', JSON.stringify(shipper), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  // HttpClient API delete() method
  deleteShipper(id:number){
    return this.http.delete<Shipper>(this.apiURL + id, this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

// Error handling
handleError(error:any) {
  let errorMessage = '';
  if(error.error instanceof ErrorEvent) {
    // Get client-side error
    errorMessage = error.error.message;
  } else {
    // Get server-side error
    errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
  }
  window.alert(errorMessage);
  return throwError(errorMessage);
}

}




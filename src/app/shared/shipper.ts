export class Shipper {
    id: number;
    companyName: string;
    phone: string;
    
    constructor(){
        this.id = 0;
        this.companyName = "";
        this.phone="";
    }
}

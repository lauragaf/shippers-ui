import { Component, Input, OnInit } from '@angular/core';
import { RestApiService } from '../shared/rest-api.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-shipper-create',
  templateUrl: './shipper-create.component.html',
  styleUrls: ['./shipper-create.component.css']
})
export class ShipperCreateComponent implements OnInit {

  @Input() shipperDetails = {id:0, companyName: '', phone: ''}
  constructor(
    public restApi: RestApiService,
    public router: Router

  ) { }

  ngOnInit(): void {
  }

  addShipper(){
    this.restApi.createShipper(this.shipperDetails).subscribe((data: {})=>
    {this.router.navigate(['/shipper-list'])
  })
}
}
